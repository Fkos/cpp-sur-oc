#ifndef CALCCPPLIB_H_INCLUDED
#define CALCCPPLIB_H_INCLUDED

double add(double arg1, double arg2);

double subtract(double arg1, double arg2);

double product(double arg1, double arg2);

double divide(double arg1, double arg2);

int modulo(int arg1, int arg2);

#endif // CALCCPPLIB_H_INCLUDED
