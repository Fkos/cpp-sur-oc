#include <iostream>
#include "calcpplib.h"

using namespace std;

int main(){
	bool reoperate(true);
	char confirmReoperate;
	unsigned int operation(0);
	double operand1,operand2;
	do {
		cout << "Welcome to Calc++\nChoose your operation :" << endl;
		cout << "1 : addition\n2 : subtraction\n3 : product\n4 : division\n5 : modulo" << endl;
		cin >> operation;
		cout << "first operand : ";
		cin >> operand1;
		cout << "second operand : ";
		cin >> operand2;
		
		switch(operation){
			case 1 : 
				cout << "result : " << add(operand1,operand2);
			break;
			case 2 : 
				cout << "result : " << subtract(operand1,operand2);
			break;
			case 3 : 
				cout << "result : " << product(operand1,operand2);
			break;
			case 4 :
				cout << "result : " << divide(operand1,operand2);
			break;
			case 5 : 
				cout << "result : " << modulo((int)operand1,(int)operand2);
			break;
			default :
				cout << "invalid operation ";
			break;
		
		}
		cout << "\nPress c to continue" << endl;
		cin >> confirmReoperate;
		
		if (confirmReoperate != 'c'){
			reoperate = false;
		}
	} while (reoperate);
}
