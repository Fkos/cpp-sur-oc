#include "calcpplib.h"

double add(double arg1,double arg2){
	return arg1 + arg2;
}

double subtract(double arg1,double arg2){
	return arg1 - arg2;
}

double product(double arg1,double arg2){
	return arg1 * arg2;
}

double divide(double arg1,double arg2){
	if (arg2 != 0){
		return arg1/arg2;
	}
	return 0;
}

int modulo(int arg1, int arg2){
	return arg1%arg2;
}
