#include <iostream>
#include <stdlib.h>
#include <time.h>  
#include <fstream>

using namespace std;

string mixWord(string word){
    string mixed("");
    int length(word.length());
    for(int i=0;i<length;i++){
        int pos = rand() % word.length() + 1;
        mixed += word.at(pos-1);
        word = word.erase(pos-1,1);
    }
    return mixed;
}


int main(){
    cout << "Chargement" << endl ;

    string const filepath("./files/dictionnary.txt");
    string mysteryWord;
    string attempt;
    srand(time(NULL));
    int cursor = rand() % 25;
    int retry(3);
    ifstream dictionnary(filepath);
    if(dictionnary){
        int i(0);
        while(getline(dictionnary,mysteryWord)){
            if(i==cursor){
                break;
            }
            i++;
        }

        cout << "Vous devez trouver le mot mystere : " << mixWord(mysteryWord) << endl  << endl ;
        while(retry > 0) {
            cin >> attempt;
            if(mysteryWord.compare( attempt ) == 0){
                cout  << "Vous avez trouve le mot mystere !!! Félicitation" << endl ;
                break;
            }

            retry--;

            cout  << "Tout faux... Il vous reste "  << retry << " tentatives" << endl ;
        }
    }else{
        cout << "Impossible de charger le dictionnaire" << endl ;
    }
    return 0;
}